// AdminLTE Gruntfile
module.exports = function (grunt) { // jshint ignore:line
  'use strict';

  var static_adminlte = 'django_adminlte/static/adminlte/';
  var static_bootstrap = 'django_adminlte/static/bootstrap/';
  var static_fontawesome = 'django_adminlte/static/font-awesome/';
  var static_ionicons = 'django_adminlte/static/ionicons/';

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    // Configuration to be run (and then tested).
    rtlcss: {
      all: {
        options: {
          map: {
            inline: false
          },
          opts: {
            clean: false
          },
          saveUnmodified: true
        },
        files: {
          'dist/css/AdminLTE.rtl.css': 'node_modules/admin-lte/dist/css/AdminLTE.css',
          'dist/css/AdminLTE.rtl.min.css': 'node_modules/admin-lte/dist/css/AdminLTE.min.css'
        }
      }
    },

    clean: {
      django: [
        static_adminlte,
        static_bootstrap,
        static_fontawesome,
        static_ionicons,
        // python setup.py stuff
        'build/',
        'dist/*.{whl,zip,tar,tar.gz}'
      ],
    },

    copy: {
      django: {
        options: {
          timestamp: true // to preserve files timestamp
        },
        files: [
          { // LTR files
            dest: static_adminlte,
            src: ['node_modules/admin-lte/dist/**'],
            expand: true,
            filter: 'isFile',
            rename: function (dest, src) {
              return dest + src.replace('node_modules/admin-lte/dist/', '').toLowerCase();
            }
          },
          { // RTL files
            dest: static_adminlte,
            src: ['dist/css/**'],
            expand: true,
            filter: 'isFile',
            rename: function (dest, src) {
              return dest + src.replace('dist/', '').toLowerCase();
            }
          },
          { // bootstrap files
            dest: static_bootstrap,
            src: ['node_modules/bootstrap/dist/**'],
            expand: true,
            filter: 'isFile',
            rename: function (dest, src) {
              return dest + src.replace('node_modules/bootstrap/dist/', '').toLowerCase();
            }
          },
          { // bootstrap-rtl files
            dest: static_bootstrap,
            src: ['node_modules/bootstrap-rtl/dist/css/**'],
            expand: true,
            filter: 'isFile',
            rename: function (dest, src) {
              return dest + src.replace('node_modules/bootstrap-rtl/dist/', '').toLowerCase();
            }
          },
          {// font-awesome
            dest: static_fontawesome,
            src: ['node_modules/font-awesome/css/**', 'node_modules/font-awesome/fonts/**',],
            expand: true,
            filter: 'isFile',
            rename: function (dest, src) {
              return dest + src.replace('node_modules/font-awesome/', '').toLowerCase();
            }
          },
          {// ionicons
            dest: static_ionicons,
            src: ['node_modules/ionicons/dist/css/**', 'node_modules/ionicons/dist/fonts/**',],
            expand: true,
            filter: 'isFile',
            rename: function (dest, src) {
              return dest + src.replace('node_modules/ionicons/dist/', '').toLowerCase();
            }
          },
        ],
      },
    }
  })
  ;

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-rtlcss');

  // CSS Task
  grunt.registerTask('default', ['rtlcss']);
  grunt.registerTask('django', ['rtlcss', 'clean:django', 'copy:django']);

};
