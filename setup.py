#

from setuptools import setup, find_packages

setup(
    name='django-adminlte-rtl',
    version='2.4.0.1',

    url='https://gitlab.com/tayyeb/adminLTE-rtl.git',
    author='Tayyeb',
    author_email='tayyeb@tsaze.com',

    license='MIT',

    packages=find_packages(),
    include_package_data=True,

    setup_requires=['setuptools_scm', ],
)
